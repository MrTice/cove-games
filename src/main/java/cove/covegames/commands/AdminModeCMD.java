package cove.covegames.commands;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import cove.covegames.Main;

public class AdminModeCMD implements CommandExecutor{
	Main plugin;
	
	public static Set<String> adminMode = new HashSet<String>();

	public AdminModeCMD(Main main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player player = (Player) sender;
		if(player.hasPermission("cove.admin")){
			if(cmd.getName().equalsIgnoreCase("adminmode")){
				if(!adminMode.contains(player.getUniqueId().toString().replace("-", ""))){
					adminMode.add(player.getUniqueId().toString().replace("-", ""));
					player.sendMessage(plugin.prefix + ChatColor.YELLOW + "Admin mode: " + ChatColor.GREEN + "enabled");
				}
				else{
					adminMode.remove(player.getUniqueId().toString().replace("-", ""));
					player.sendMessage(plugin.prefix + ChatColor.YELLOW + "Admin mode: " + ChatColor.RED + "disabled");
				}
			}
		} else{
			player.sendMessage(plugin.prefix + ChatColor.RED + "This command is only available for Administrators.");
		}
		
		return false;
	}

}
