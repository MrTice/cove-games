package cove.covegames.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import cove.covegames.Main;
import cove.covegames.gamestates.End;

public class EndGameCMD implements CommandExecutor{
	Main plugin;
	public End end;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel,String[] args) {
		Player player = (Player) sender;
		if(player.hasPermission("cove.admin")){
			if(plugin.timer < 1800){
			if(cmd.getName().equalsIgnoreCase("endgame")){
				end.endTimer = -1;
				plugin.getServer().getScheduler().runTaskTimer(plugin, plugin.endGameState, 0, 20);
			}
			
		} else{
			player.sendMessage(plugin.prefix + ChatColor.RED + ChatColor.ITALIC + "You are in the lobby, you can't end it now because it hasn't begun!");
		}
		}
		return false;
	}

}
