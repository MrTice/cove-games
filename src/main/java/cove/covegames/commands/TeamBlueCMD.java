package cove.covegames.commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;





import cove.covegames.Main;

public class TeamBlueCMD implements CommandExecutor{
	
	FileConfiguration blueSpawns;
	FileConfiguration firstSpawn;
	
	int spawnNumber = 1;
	Main plugin;

	public TeamBlueCMD(Main main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player player = (Player)sender;
		if(player.hasPermission("cove.admin")){
			if(cmd.getName().equalsIgnoreCase("addbluespawn")){
				File locationDirectory = new File(plugin.getDataFolder() + File.separator + "TeamBlue");
				File locationConfig = new File(plugin.getDataFolder() + File.separator + "TeamBlue" + File.separator + "BlueSpawns.yml");
				
				//Checking if config exists, if not, creates it and its directories.
				if(locationConfig.exists()){
					player.sendMessage(plugin.prefix + ChatColor.GRAY + "Config already exists!");
				} else{
					try{
						locationDirectory.mkdirs();
						locationConfig.createNewFile();
						player.sendMessage(plugin.prefix + ChatColor.GRAY + "Config successfully created");
					} catch(IOException e){
						player.sendMessage(plugin.prefix + ChatColor.RED + "Failed to create the config!");
						e.printStackTrace();
					}
				}
				//Loading the config:
				blueSpawns = YamlConfiguration.loadConfiguration(locationConfig);
				if(blueSpawns != null){
					player.sendMessage("Config loaded.");
				} else{
					player.sendMessage("Can't find config.");
				}
				//Writing information to the config:
				float pitch = player.getEyeLocation().getPitch();
				float yaw = player.getEyeLocation().getYaw();
				blueSpawns.set(spawnNumber + ".x", player.getLocation().getX());
				blueSpawns.set(spawnNumber + ".y", player.getLocation().getY());
				blueSpawns.set(spawnNumber + ".z", player.getLocation().getZ());
				blueSpawns.set(spawnNumber + ".pitch", pitch);
				blueSpawns.set(spawnNumber + ".yaw", yaw);
				//Saving the config:
				try{
					blueSpawns.save(locationConfig);
					player.sendMessage(plugin.prefix + ChatColor.YELLOW + "Blue spawn " + ChatColor.BLUE + spawnNumber + " set!");
					spawnNumber++;
				} catch(IOException e){
					player.sendMessage("ERROR WITH SAVING, BAAAD TICE");
					e.printStackTrace();
				}
			}
			if(cmd.getName().equalsIgnoreCase("firstbluespawn")){
				File locationDirectory = new File(plugin.getDataFolder() + File.separator + "TeamBlue");
				File locationConfig = new File(plugin.getDataFolder() + File.separator + "TeamBlue" + File.separator + "FirstSpawn.yml");
				//Checking if config exists, if not, it creates its directories.
				if(locationConfig.exists()){
					player.sendMessage(plugin.prefix + ChatColor.GRAY + "Config already exists!");
				} else{
					try{
						locationDirectory.mkdirs();
						locationConfig.createNewFile();
						player.sendMessage(plugin.prefix + ChatColor.GRAY + "Config successfully created");
					} catch(IOException e){
						player.sendMessage(plugin.prefix + ChatColor.RED + "Failed to create the config!");
						e.printStackTrace();
					}
				}
				
				//Loading the config:
				firstSpawn = YamlConfiguration.loadConfiguration(locationConfig);
				if(firstSpawn != null){
					player.sendMessage("Config loaded.");
				} else{
					player.sendMessage("Couldn't find config.");
				}
				//Writing information to the config:
				float pitch = player.getEyeLocation().getPitch();
				float yaw = player.getEyeLocation().getYaw();
				String spawn = "Spawn";
				firstSpawn.set(spawn + ".x", player.getLocation().getX());
				firstSpawn.set(spawn + ".y", player.getLocation().getY());
				firstSpawn.set(spawn + ".z", player.getLocation().getZ());
				firstSpawn.set(spawn + ".pitch", pitch);
				firstSpawn.set(spawn + ".yaw", yaw);
				//Saving the config:
				try{
					firstSpawn.save(locationConfig);
					player.sendMessage(plugin.prefix + ChatColor.YELLOW + "The primary spawn inwhich the players get teleported too when the game turns to ingame was set!");
				} catch(IOException e){
					player.sendMessage("Something with saving the file went wrong.... oops");
					e.printStackTrace();
				}
			}
			
			
			
		} else{
			player.sendMessage(plugin.prefix + ChatColor.RED + "This command is only available for Administrators.");
		}
		return false;
	}
	
	

}
