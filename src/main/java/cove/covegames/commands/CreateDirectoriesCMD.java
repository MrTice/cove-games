package cove.covegames.commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import cove.covegames.Main;

public class CreateDirectoriesCMD implements CommandExecutor{
	Main plugin;

	public CreateDirectoriesCMD(Main main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player player = (Player)sender;
		if(player.hasPermission("cove.admin")){
			if(cmd.getName().equalsIgnoreCase("createdirs")){
				File blueDirectory;
				File redDirectory;
				blueDirectory = new File(plugin.getDataFolder() + File.separator + "TeamBlue");
				File blueSpawns = new File(plugin.getDataFolder() + File.separator + "TeamBlue" + File.separator + "BlueSpawns.yml");
				File blueFirstSpawn = new File(plugin.getDataFolder() + File.separator + "TeamBlue" + File.separator + "FirstSpawn.yml");
				redDirectory = new File(plugin.getDataFolder() + File.separator + "TeamRed");
				File redSpawns = new File(plugin.getDataFolder() + File.separator + "TeamRed" + File.separator + "RedSpawns.yml");
				File redFirstSpawn = new File(plugin.getDataFolder() + File.separator + "TeamRed" + File.separator + "FirstSpawn.yml");
				if(configExists(blueDirectory) == false){
					try{
						blueDirectory.mkdirs();
						blueSpawns.createNewFile();
						blueFirstSpawn.createNewFile();
						redDirectory.mkdirs();
						redSpawns.createNewFile();
						redFirstSpawn.createNewFile();
						player.sendMessage(plugin.prefix + ChatColor.YELLOW + "Successfully created the files and directories.");
					} catch(IOException e ){
						player.sendMessage("Error when creating and saving the config");
						e.printStackTrace();
					}
				}
				
			}
		}
		return false;
	}
	
	public boolean configExists(File file){
		//Checking if the config exists
		if(!file.exists()){
			return false;
		} else{
			return true;
		}
	}

}
