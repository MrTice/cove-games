package cove.covegames.commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import cove.covegames.Main;

public class SetShopCMD implements CommandExecutor{
	Main plugin;
	FileConfiguration blueShopSpawn;
	FileConfiguration redShopSpawn;

	public SetShopCMD(Main main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel,String[] args) {
		Player player = (Player)sender;
        if(!player.hasPermission("cove.admin")){
            player.sendMessage(plugin.noperm);
            return false;
        }
		if(cmd.getName().equalsIgnoreCase("setshop")){
			if(args.length == 1){
				if(args[0].equalsIgnoreCase("blue")){
					File locationDirectory = new File(plugin.getDataFolder() + File.separator + "TeamBlue");
					File locationConfig = new File(plugin.getDataFolder() + File.separator + "TeamBlue" + File.separator + "ShopSpawn.yml");
					//Checking if config exists, if not, creates its directories.
					if(locationConfig.exists()){
						player.sendMessage(plugin.prefix + ChatColor.GRAY + "Config already exists!");
					} else{
						try{
							locationDirectory.mkdirs();
							locationConfig.createNewFile();
							player.sendMessage(plugin.prefix + ChatColor.GRAY + "Config successfully created");
						} catch(IOException e){
							player.sendMessage(plugin.prefix + ChatColor.RED + "Failed to create the config!");
							e.printStackTrace();
						}
					}
					//Loading config:
					blueShopSpawn = YamlConfiguration.loadConfiguration(locationConfig);
					if(blueShopSpawn != null){
						player.sendMessage("Config loaded.");
					} else{
						player.sendMessage("Couldn't find config");
					}
					//Writing information to the config:
					float pitch = player.getEyeLocation().getPitch();
					float yaw = player.getEyeLocation().getYaw();
					String spawn = "Spawn";
					blueShopSpawn.set(spawn + ".x", player.getLocation().getX());
					blueShopSpawn.set(spawn + ".y", player.getLocation().getY());
					blueShopSpawn.set(spawn + ".z", player.getLocation().getZ());
					blueShopSpawn.set(spawn + ".pitch", pitch);
					blueShopSpawn.set(spawn + ".yaw", yaw);
					//Saving the config:
					try{
						blueShopSpawn.save(locationConfig);
						player.sendMessage(plugin.prefix + ChatColor.YELLOW + "The blue shop spawn was set.");
					} catch(IOException e){
						player.sendMessage(plugin.prefix + "Something went wrong while saving the file... oops - Tice");
						e.printStackTrace();
					}
				} else if(args[1].equalsIgnoreCase("red")){
					File locationDirectory = new File(plugin.getDataFolder() + File.separator + "TeamRed");
					File locationConfig = new File(plugin.getDataFolder() + File.separator + "TeamRed" + File.separator + "ShopSpawn.yml");
					//Checking if config exists, if not, creates its directories.
					if(locationConfig.exists()){
						player.sendMessage(plugin.prefix + ChatColor.GRAY + "Config already exists!");
					} else{
						try{
							locationDirectory.mkdirs();
							locationConfig.createNewFile();
							player.sendMessage(plugin.prefix + ChatColor.GRAY + "Config successfully created");
						} catch(IOException e){
							player.sendMessage(plugin.prefix + ChatColor.RED + "Failed to create the config!");
							e.printStackTrace();
						}
					}
					//Loading config:
					redShopSpawn = YamlConfiguration.loadConfiguration(locationConfig);
					if(redShopSpawn != null){
						player.sendMessage("Config loaded.");
					} else{
						player.sendMessage("Couldn't find config");
					}
					//Writing information to the config:
					float pitch = player.getEyeLocation().getPitch();
					float yaw = player.getEyeLocation().getYaw();
					String spawn = "Spawn";
					redShopSpawn.set(spawn + ".x", player.getLocation().getX());
					redShopSpawn.set(spawn + ".y", player.getLocation().getY());
					redShopSpawn.set(spawn + ".z", player.getLocation().getZ());
					redShopSpawn.set(spawn + ".pitch", pitch);
					redShopSpawn.set(spawn + ".yaw", yaw);
					//Saving the config:
					try{
						redShopSpawn.save(locationConfig);
						player.sendMessage(plugin.prefix + ChatColor.YELLOW + "The red shop spawn was set.");
					} catch(IOException e){
						player.sendMessage(plugin.prefix + "Something went wrong while saving the file... oops - Tice");
						e.printStackTrace();
					}
				}
			}
		}
		return false;
	}

}
