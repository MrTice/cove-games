package cove.covegames.commands;

import java.io.File;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import cove.covegames.Main;
import cove.covegames.teams.Team;

public class StartCMD implements CommandExecutor {
	Main plugin;

	public StartCMD(Main main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] arg3) {
		Player player = (Player) sender;
		if(player.hasPermission("cove.admin")){
			if(cmd.getName().equalsIgnoreCase("start")){
				plugin.lobbyTime = -1;
				plugin.getServer().getScheduler().runTaskTimer(plugin, plugin.ingameState, 0, 20);
				plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
					public void run(){
						for(Player onlinePlayers : Bukkit.getOnlinePlayers()){
							if(Team.teamBlue.contains(onlinePlayers.getName())){
								FileConfiguration blueSpawn;
								File locationFile;
								locationFile = new File(plugin.getDataFolder() + File.separator + "TeamBlue" + File.separator + "FirstSpawn.yml");
								blueSpawn = YamlConfiguration.loadConfiguration(locationFile);
								String spawn = "Spawn";
								
								Double x = blueSpawn.getDouble(spawn + ".x");
								Double y = blueSpawn.getDouble(spawn + ".y");
								Double z = blueSpawn.getDouble(spawn + ".z");
								float p = (float) blueSpawn.getLong(spawn + "pitch");
								float f = (float) blueSpawn.getLong(spawn + ".yaw");
								
								onlinePlayers.teleport(new Location(Bukkit.getWorld("world"), x, y, z, f, p));
							} else if(Team.teamRed.contains(onlinePlayers.getName())){
								FileConfiguration redSpawn;
								File locationFile;
								locationFile = new File(plugin.getDataFolder() + File.separator + "TeamRed" + File.separator + "FirstSpawn.yml");
								redSpawn = YamlConfiguration.loadConfiguration(locationFile);
								String spawn = "Spawn";
								
								Double x = redSpawn.getDouble(spawn + ".x");
								Double y = redSpawn.getDouble(spawn + ".y");
								Double z = redSpawn.getDouble(spawn + ".z");
								float p = (float) redSpawn.getLong(spawn + "pitch");
								float f = (float) redSpawn.getLong(spawn + ".yaw");
								
								onlinePlayers.teleport(new Location(Bukkit.getWorld("world"), x, y, z, f, p));
								
								
							}
							onlinePlayers.getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
							onlinePlayers.getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
							onlinePlayers.getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
							onlinePlayers.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
						}
					}
				}, 0);
			}
		}
		return false;
	}
	

}
