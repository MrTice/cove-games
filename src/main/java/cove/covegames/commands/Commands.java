package cove.covegames.commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import cove.covegames.Main;

public class Commands implements CommandExecutor{
	
	Main plugin;
	

	public Commands(Main main) {
		plugin = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player player = (Player) sender;
		
		if(player.hasPermission("cove.admin")){
			if(cmd.getName().equalsIgnoreCase("setlobby")){
			FileConfiguration lobbySpawn;
			File locationFile;
			String lobby = "lobby";
			locationFile = new File(plugin.getDataFolder() + "lobbySpawn.yml");
			lobbySpawn = YamlConfiguration.loadConfiguration(locationFile);
			
			float pitch = player.getEyeLocation().getPitch();
			float yaw = player.getEyeLocation().getYaw();
			lobbySpawn.set(lobby + ".x", player.getLocation().getX());
			lobbySpawn.set(lobby + ".y", player.getLocation().getY());
			lobbySpawn.set(lobby + ".z", player.getLocation().getZ());
			lobbySpawn.set(lobby + ".pitch", pitch);
			lobbySpawn.set(lobby + ".yaw", yaw);
			
			try {
				lobbySpawn.save(locationFile);
				player.sendMessage(plugin.prefix + ChatColor.YELLOW + "Lobby spawn successfully created!");
			} catch (IOException e) {
				player.sendMessage(plugin.prefix + ChatColor.RED + "Creating config failed!");
				e.printStackTrace();
			}
			
			
			
			
			
			
		}
		if(cmd.getName().equalsIgnoreCase("setcenter")){
			FileConfiguration mapCenter;
			File locationFile;
			String center = "center";
			locationFile = new File(plugin.getDataFolder() + "mapCenter.yml");
			mapCenter = YamlConfiguration.loadConfiguration(locationFile);
			
			float pitch = player.getEyeLocation().getPitch();
			float yaw = player.getEyeLocation().getYaw();
			mapCenter.set(center + ".x", player.getLocation().getX());
			mapCenter.set(center + ".y", player.getLocation().getY());
			mapCenter.set(center + ".z", player.getLocation().getZ());
			mapCenter.set(center + ".pitch", pitch);
			mapCenter.set(center + ".yaw", yaw);
			
			try{
				mapCenter.save(locationFile);
				player.sendMessage(plugin.prefix + ChatColor.YELLOW + "Map center successfully created!");
			} catch(IOException e){
				player.sendMessage(plugin.prefix + ChatColor.RED + "Creating config failed!");
				e.printStackTrace();
			}
			
			
			
			
			
		}
		
		} else{
			player.sendMessage(plugin.prefix + ChatColor.RED + "This command is only available for Administrators.");
		}
		
		
		return false;
	}

}
