package cove.covegames;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import cove.covegames.gamestates.listeners.LobbyListener;
import cove.covegames.teams.Team;
import cove.covegames.parties.Party;
import cove.covegames.parties.PartyHandler;

public class DeBuggingRunnable extends BukkitRunnable{
	Main plugin;
	Team team;
	Party party;
	private PartyHandler partyHandler = new PartyHandler(plugin);
	private LobbyListener lobbyListener = new LobbyListener(plugin);
	public DeBuggingRunnable(Main main) {
		plugin = main;
	}

	@Override
	public void run() {
		System.out.println("BLUE TEAM SIZE: " + Team.teamBlue.size());
		System.out.println("RED TEAM SIZE: " + Team.teamRed.size());
		System.out.println("NUMNBER OF PARTIES: " + partyHandler.parties.size());
		System.out.println("NUMBER OF hasCreatedParty ENTRIES:" + lobbyListener.hasCreatedParty.size());
		
		for(Player players : Bukkit.getOnlinePlayers()){
			if(Team.teamBlue.contains(players.getName())){
				System.out.println("BLUE TEAM PLAYERS: " + players.getName());
			}
			if(Team.teamRed.contains(players.getName())){
				System.out.println("RED TEAM PLAYERS: " + players.getName());
			}
		}
		
	}

}
