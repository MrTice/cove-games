package cove.covegames.progress;

import java.util.HashMap;

/**
 * Created by Teodor on 2014-05-25.
 */
public class PointSystem {
    HashMap<String, Integer> playerPoints = new HashMap<String, Integer>();

    public void addPoints(String playerName, Integer points){
        playerPoints.put(playerName, points);
    }
    public void removePoints(String playerName, Integer points){
        playerPoints.put(playerName, points);
    }
    public void removePlayer(String playerName){
        playerPoints.remove(playerName);
    }
    public Integer getPoints(String playerName){
        return playerPoints.get(playerName);
    }
}
