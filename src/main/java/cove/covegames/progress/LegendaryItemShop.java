package cove.covegames.progress;

import cove.covegames.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

/**
 * Created by Teodor on 2014-05-26.
 */
public class LegendaryItemShop implements Listener{
    PointSystem pointSystem;
    Main plugin;

        public void generateShop(){
            plugin.legendaryShopInventory = Bukkit.getServer().createInventory(null, 9, "Legendary Items");
            plugin.legendaryShopInventory.setItem(0, thorAxe());
            plugin.legendaryShopInventory.setItem(1, bloodTooth());
            plugin.legendaryShopInventory.setItem(2, grapple());

        }

    public ItemStack thorAxe(){
        ArrayList<String> lores = new ArrayList<String>();
        lores.add(ChatColor.AQUA + "- Striking your foes has a " + ChatColor.GREEN + ChatColor.BOLD + "10%" + ChatColor.AQUA + " chance");
        lores.add(ChatColor.AQUA + "of dealing extra lightning damage.");
        ItemStack item = new ItemStack(Material.DIAMOND_AXE);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("" + ChatColor.RED + ChatColor.BOLD + "Thor's Axe");
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;
    }
    public ItemStack bloodTooth(){
        ArrayList<String> lores = new ArrayList<String>();
        lores.add(ChatColor.AQUA + "- When hitting your enemies, there " + ChatColor.GREEN + ChatColor.BOLD + "10%" + ChatColor.AQUA + " chance");
        lores.add(ChatColor.AQUA + "is a " + ChatColor.GREEN + ChatColor.BOLD + "30%" + ChatColor.AQUA + " chance that you will get healed");
        lores.add(ChatColor.AQUA + "by half a heart.");
        ItemStack item = new ItemStack(Material.IRON_SWORD);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("" + ChatColor.RED + ChatColor.BOLD + "Bloodtooth");
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;
    }
    public ItemStack grapple(){
        ArrayList<String> lores = new ArrayList<String>();
        lores.add(ChatColor.YELLOW + "- Use the grapple to move around");
        lores.add(ChatColor.YELLOW + "more effectively.");
        ItemStack item = new ItemStack(Material.FISHING_ROD);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("" + ChatColor.GOLD + ChatColor.BOLD + "Grapple");
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;

    }

    @EventHandler
    public void Handler(InventoryClickEvent event){
        Player player = (Player)event.getWhoClicked();
        event.setCancelled(true);
        if(event.getInventory().getTitle().equals("Legendary Items")){
            if(event.getCurrentItem().getType().equals(Material.DIAMOND_AXE)){
                if(pointSystem.getPoints(player.getName()) < 1){

                }
            }
        }
    }
}
