package cove.covegames.progress;

import cove.covegames.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

/**
 * Created by Teodor on 2014-05-27.
 */
public class CategoryShop {
    Main plugin;
    PointSystem pointSystem;

    public void generateShop(){
        plugin.categoryShopInventory = Bukkit.createInventory(null, 9, "Category");
        plugin.categoryShopInventory.setItem(0, legendaryItem());

    }

    public ItemStack legendaryItem(){
        ArrayList<String> lores = new ArrayList<String>();
        lores.add("" + ChatColor.BLUE + "Weapons with special abilities");
        ItemStack item = new ItemStack(Material.BLAZE_ROD);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("" + ChatColor.RED + ChatColor.BOLD + "Legendary Weapons");
        meta.setLore(lores);
        item.setItemMeta(meta);
        return item;

    }
}
