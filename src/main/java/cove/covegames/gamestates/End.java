package cove.covegames.gamestates;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import cove.covegames.Main;

public class End extends BukkitRunnable{
	Main plugin;
	public int endTimer = 5;

	public End(Main main) {
		plugin = main;
	}

	

	@Override
	public void run() {
		endTimer--;
		if(endTimer == 5){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.RED + ChatColor.BOLD + "Game ending in");
		}
		if(endTimer == 4){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.RED + ChatColor.BOLD + endTimer);
		}
		if(endTimer == 3){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.RED + ChatColor.BOLD + endTimer);
		}
		if(endTimer == 2){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.RED + ChatColor.BOLD + endTimer);
		}
		if(endTimer == 1){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.RED + ChatColor.BOLD + endTimer);
		}
		if(endTimer == 0){
			Bukkit.getServer().shutdown();
		}
		
	}

}
