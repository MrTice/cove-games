package cove.covegames.gamestates.listeners;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import cove.covegames.Items;
import cove.covegames.Main;
import cove.covegames.teams.Team;
import cove.covegames.parties.Party;
import cove.covegames.parties.PartyHandler;

public class LobbyListener implements Listener{
	
	private Set<String> hasChanged = new HashSet<String>();
	public static Set<String> hasCreatedParty = new HashSet<String>();
	Main plugin;
	private Items items = new Items();
	private PartyHandler partyHandler = new PartyHandler(plugin);
	private Party party = new Party(null, 0);
	
	
	public LobbyListener(Main main) {
		plugin = main;
	}
	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent event){
		if(plugin.timer >= 1800){
			if(event.getEntity() instanceof Player && event.getDamager() instanceof Player){
				event.setCancelled(true);
			}
		} 
	}
	@EventHandler
	public void onDamage(EntityDamageEvent event){
		if(plugin.timer >= 1800){
			if(event.getCause().equals(DamageCause.FALL)){
				event.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void interact(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if(plugin.timer >= 1800){
			if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
				if(event.getMaterial().equals(Material.NETHER_STAR)){
				if(!player.hasPermission("cove.donor")){
					if(!hasChanged.contains(player.getUniqueId().toString().replace("-", ""))){
						if(Team.teamBlue.contains(player.getName())){
							if(Team.teamBlue.size() >= Team.teamRed.size()){
								if(Team.teamRed.size() != 15){
									Team.teamBlue.remove(player.getName());
									Team.teamRed.add(player.getName());
									player.sendMessage(plugin.prefix + ChatColor.YELLOW + "You have switched to the " + ChatColor.RED + ChatColor.BOLD + "red " + ChatColor.YELLOW + "team!");
									System.out.println();
									hasChanged.add(player.getUniqueId().toString().replace("-", ""));
								} else{
									player.sendMessage(plugin.prefix + ChatColor.RED + "The red team is full!");
								}
							} else{
								player.sendMessage(plugin.prefix + ChatColor.RED + "Your team needs more players!");
							}
						}
						else if(Team.teamRed.contains(player.getName())){
							if(Team.teamRed.size() >= Team.teamBlue.size()){
								if(Team.teamBlue.size() != 15){
									Team.teamRed.remove(player.getName());
							        Team.teamBlue.add(player.getName());
							        player.sendMessage(plugin.prefix + ChatColor.YELLOW + "You have switched to the " + ChatColor.BLUE + ChatColor.BOLD + "blue " + ChatColor.YELLOW + "team!");
							        hasChanged.add(player.getUniqueId().toString().replace("-", ""));
								} else{
									player.sendMessage(plugin.prefix + ChatColor.RED + "The blue team is full!");
								}
							} else{
								player.sendMessage(plugin.prefix + ChatColor.RED + "Your team needs more players!");
							}
							
						}
					} else{
						player.sendMessage(plugin.prefix + ChatColor.RED + "You have already switched cove.covegames.teams once.");
						player.sendMessage(plugin.prefix + ChatColor.YELLOW + "Want unlimited team switching? Buy Premium at" + ChatColor.GREEN + " http://shop.covenetwork.net/");
					}
				}else{
					if(player.hasPermission("cove.donor")){
						if(Team.teamBlue.contains(player.getName())){
							if(Team.teamBlue.size() >= Team.teamRed.size()){
								if(Team.teamRed.size() != 15){
									Team.teamBlue.remove(player.getName());
									Team.teamRed.add(player.getName());
									player.sendMessage(plugin.prefix + ChatColor.YELLOW + "You have switched to the " + ChatColor.RED + ChatColor.BOLD + "red " + ChatColor.YELLOW + "team!");
								} else{
									Player unLucky = getRandomRedNonDonor();
									Team.teamBlue.remove(player.getName());
									Team.teamRed.remove(unLucky.getName());
									Team.teamRed.add(player.getName());
									Team.teamBlue.add(unLucky.getName());
									unLucky.sendMessage(plugin.prefix + ChatColor.AQUA + "A premium player has switched cove.covegames.teams with you. You are now team" + ChatColor.BLUE + ChatColor.BOLD + " blue.");
									player.sendMessage(plugin.prefix + ChatColor.YELLOW + "You have switched team to the" + ChatColor.RED + ChatColor.BOLD + " red" + ChatColor.YELLOW + " team");
									
									
								}
							}
						}
					} else if(Team.teamRed.contains(player.getName())){
						if(Team.teamRed.size() >= Team.teamBlue.size()){
							if(Team.teamBlue.size() != 15){
								Team.teamRed.remove(player.getName());
								Team.teamBlue.add(player.getName());
								player.sendMessage(plugin.prefix + ChatColor.YELLOW + "You have switched to the " + ChatColor.BLUE + ChatColor.BOLD + "blue " + ChatColor.YELLOW + "team!");
								
							} else{
								Player unLucky = getRandomBlueNonDonor();
								Team.teamRed.remove(player.getName());
								Team.teamBlue.remove(unLucky.getName());
								Team.teamBlue.add(player.getName());
								Team.teamRed.add(unLucky.getName());
								unLucky.sendMessage(plugin.prefix + ChatColor.AQUA + "A premium player has switched cove.covegames.teams with you. You are now team" + ChatColor.RED + ChatColor.BOLD + " red.");
								player.sendMessage(plugin.prefix + ChatColor.YELLOW + "You have switched team to the" + ChatColor.BLUE + ChatColor.BOLD + " blue" + ChatColor.YELLOW + " team");
							}
						}
					}
				}
			}
			}
		}
		
	
	}
	
	
	@EventHandler
	public void onClickPartyItem(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if(plugin.timer >= 1800){
			if(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
				if(event.getMaterial().equals(Material.NAME_TAG)){
					if(!hasCreatedParty.contains(player.getUniqueId().toString().replace("-", ""))){
						plugin.partyInventory = Bukkit.getServer().createInventory(null, 9, "Parties");
						plugin.partyInventory.setItem(8, items.createParty());
						player.openInventory(plugin.partyInventory);
					} else{
						plugin.partyInventory = Bukkit.getServer().createInventory(null, 9, "Parties");
						plugin.partyInventory.setItem(8, items.manageParty());
						for(Party party : partyHandler.parties.values()){
							ItemStack item = new ItemStack(Material.BOWL);
							ItemMeta meta = item.getItemMeta();
							meta.setDisplayName(party.getName());
							item.setItemMeta(meta);
							plugin.partyInventory.setItem(1 - 1, new ItemStack(Material.BOWL));
						}
						
						player.openInventory(plugin.partyInventory);
					}
				}
			}
		}
	}
	@EventHandler
	public void partyClickerHandler(InventoryClickEvent event){
		Player player = (Player) event.getWhoClicked();
		if(event.getInventory().getTitle().equals("Parties")){
			if(event.getCurrentItem().getType().equals(Material.CAULDRON_ITEM)){
				if(hasCreatedParty.contains(player.getUniqueId().toString().replace("-", ""))){
					player.closeInventory();
					player.openInventory(plugin.managePartyInventory);
				} else{
					player.closeInventory();
					partyHandler.addParty(player.getName() + "'s party", 3);
					player.sendMessage(plugin.prefix + ChatColor.GREEN + "You have created your party!");
					hasCreatedParty.add(player.getUniqueId().toString().replace("-", ""));
					player.sendMessage("CREATED A PARTY");
				}
			}
					
					
					
				
			
		}
		if(event.getInventory().getTitle().equals("Manage Party")){
			if(event.getCurrentItem().getType().equals(Material.EMERALD)){
				for(Player onlinePlayers : Bukkit.getOnlinePlayers()){
					player.closeInventory();
					Inventory invitePlayers;
					int onlinePlayerMultiplication = Bukkit.getOnlinePlayers().length + 2 / 9;
					int finalAmount = 9 + (onlinePlayerMultiplication * 9);
					invitePlayers = Bukkit.getServer().createInventory(null, finalAmount, ChatColor.GREEN + "Invite Players");
					player.openInventory(invitePlayers);
					
					
					
					ItemStack skull = new ItemStack(Material.SKULL_ITEM);
					SkullMeta meta = (SkullMeta) skull.getItemMeta();
					meta.setOwner(onlinePlayers.getName());
					skull.setItemMeta(meta);
					ItemStack back = new ItemStack(Material.BOOK);
					ItemMeta meta2 = back.getItemMeta();
					meta2.setDisplayName("" + ChatColor.RED + ChatColor.BOLD + "Back");
					back.setItemMeta(meta2);
					
					invitePlayers.setItem(8, back);
					invitePlayers.setItem(finalAmount - 1, skull);
					
					
				}
			}
			if(event.getCurrentItem().getType().equals(Material.BED)){
				player.closeInventory();
				partyHandler.removeParty(player.getName() + "'s party");
				player.sendMessage(plugin.prefix + ChatColor.RED + "You've deleted your party!");
			}
			if(event.getCurrentItem().getType().equals(Material.SKULL)){
				player.closeInventory();
				if(party.getPlayerNames().contains(partyHandler.getPlayersParty(player))){
					for(Player players : party.getPlayers()){
						Inventory playersToKick;
						playersToKick = Bukkit.getServer().createInventory(null, 9, "Players in your party");
						ItemStack skull = new ItemStack(Material.SKULL_ITEM);
						SkullMeta meta = (SkullMeta) skull.getItemMeta();
						meta.setOwner(party.getPlayerNames().toString());
						skull.setItemMeta(meta);
						meta.setDisplayName(party.getPlayerNames().toString());
						playersToKick.setItem(- 1, skull);
						
					}
				}
			}
			if(event.getCurrentItem().getType().equals(Material.BOOK)){
				player.closeInventory();
				
			}
		}
		if(event.getInventory().getTitle().equals(ChatColor.GREEN + "Invite Players")){
			if(event.getCurrentItem().getType().equals(Material.BOOK)){
				player.closeInventory();
				player.openInventory(plugin.managePartyInventory);
			}
		}
		
	}
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent event){
		if(plugin.timer >= 1800){
			event.setCancelled(true);
		}
	}
	@EventHandler
	public void onItemDrop(PlayerDropItemEvent event){
		if(plugin.timer >= 1800){
			event.setCancelled(true);
		}
	}
	
	public void partyInventoryGenerator(){
		plugin.partyInventory = Bukkit.getServer().createInventory(null, 9, "Parties");
		plugin.partyInventory.setItem(9, items.createParty());
		
	}
	
	public Player getRandomBlueNonDonor(){
		Player p = null;
		ArrayList<Player> nondonors = new ArrayList<Player>();
		for(String s : Team.teamBlue){
			Player player = getPlayer(s);
			if(!player.hasPermission("cove.donor")){
				nondonors.add(player);
			}
		}
		if(!nondonors.isEmpty()){
			Random random = new Random();
			int rI = random.nextInt(nondonors.size());
			p = nondonors.get(rI);
		}
		return p;
	}
	public Player getRandomRedNonDonor(){
		Player p = null;
		ArrayList<Player> nondonors = new ArrayList<Player>();
		for(String s : Team.teamRed){
			Player player = getPlayer(s);
			if(!player.hasPermission("cove.donor")){
				nondonors.add(player);
			}
		}
		if(!nondonors.isEmpty()){
			Random random = new Random();
			int rI = random.nextInt(nondonors.size());
			p = nondonors.get(rI);
		}
		return p;
	}
	
	public Player getPlayer(String s){
		Player p = null;
		for(Player pl : Bukkit.getOnlinePlayers()){
			if(pl.getName().equalsIgnoreCase(s)){
				p = pl;
			}
		}
		return p;
		
	}
	

}
