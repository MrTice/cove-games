package cove.covegames.gamestates.listeners;

import java.io.File;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.CreatureType;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import com.bergerkiller.bukkit.common.events.CreaturePreSpawnEvent;

import cove.covegames.Main;
import cove.covegames.commands.AdminModeCMD;
import cove.covegames.teams.Team;

public class InGameListener implements Listener{
	Main plugin;
	AdminModeCMD adminModeCMD;
	
	public InGameListener(Main main) {
		plugin = main;
	}
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event){
		if(plugin.timer < 1800){
			if(event.getInventory().getTitle().equalsIgnoreCase("Shop")){
				event.setCancelled(true);
			}else{
				event.setCancelled(false);
			}
		} else{
			if(adminModeCMD.adminMode.contains(event.getWhoClicked().getUniqueId().toString().replace("-", ""))){
				event.setCancelled(false);
			} else{
				event.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void onCreatureSpawn(CreaturePreSpawnEvent event){
        event.setCancelled(true);
	}
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent event){
		if(plugin.timer < 1800){//IF IT IS INGAME
			if(event.getEntity() instanceof Player && event.getDamager() instanceof Player){
				Player victim  = (Player) event.getEntity();
				Player attacker = (Player) event.getDamager();
				if(Team.teamBlue.contains(attacker.getName()) && Team.teamRed.contains(victim.getName())){
					event.setCancelled(false);
				} else if(Team.teamBlue.contains(attacker.getName()) && Team.teamBlue.contains(victim.getName())){
					event.setCancelled(true);
					attacker.sendMessage(plugin.prefix + ChatColor.RED + "You cannot hit your teammate!");
				} else if(Team.teamRed.contains(attacker.getName()) && Team.teamBlue.contains(victim.getName())){
					event.setCancelled(false);
				} else if(Team.teamRed.contains(attacker.getName()) && Team.teamRed.contains(victim.getName())){
					event.setCancelled(true);
					attacker.sendMessage(plugin.prefix + ChatColor.RED + "You cannot hit your teammate!");
				}
			}
		} 
	}
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		Player player = event.getPlayer();
		if(plugin.timer < 1800){
			if(adminModeCMD.adminMode.contains(player.getUniqueId().toString().replace("-", ""))){
				event.setCancelled(false);
			} else{
				if(event.getBlock().getType().equals(Material.HAY_BLOCK)){
					event.setCancelled(false);
					player.sendMessage(plugin.prefix + ChatColor.GREEN + "You destroyed a hay block, and earned (SOMETHING)");
				} else{
					event.setCancelled(true);
				}
			}
		}
	}
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event){
		Player player = event.getPlayer();
		if(plugin.timer < 1800){
			if(adminModeCMD.adminMode.contains(player.getUniqueId().toString().replace("-", ""))){
				event.setCancelled(false);
			} else{
				event.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent event){
		Player player = event.getEntity();
		Player killer = player.getKiller();
		killer.sendMessage(plugin.prefix + "");
		if(event.getEntityType().equals(EntityType.PLAYER)){
			event.setDeathMessage(null);
		}
	}
	@EventHandler
	public void onRespawn(PlayerRespawnEvent event){
		Player player = event.getPlayer();
		if(Team.teamBlue.contains(player.getName())){
			File fileLocation = new File(plugin.getDataFolder() + File.separator + "TeamBlue" + File.separator + "BlueSpawns.yml");
			FileConfiguration blueSpawns;
			blueSpawns = YamlConfiguration.loadConfiguration(fileLocation);
			Random rand = new Random();
			int i1 = rand.nextInt(10) + 1;
			Double x = blueSpawns.getDouble(i1 + ".x");
			Double y = blueSpawns.getDouble(i1 + ".y");
			Double z = blueSpawns.getDouble(i1 + ".z");
			float p = (float) blueSpawns.getLong(i1 + ".pitch");
			float f = (float) blueSpawns.getLong(i1 + ".yaw");
			Location spawn = event.getRespawnLocation();
			spawn.setX(x);
			spawn.setY(y);
			spawn.setZ(z);
			spawn.setPitch(p);
			spawn.setYaw(f);
			event.setRespawnLocation(spawn);
			player.getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
			player.getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
			player.getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
			player.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
			player.getInventory().setItem(0, new ItemStack(Material.WOOD_SWORD));
		}
		if(Team.teamRed.contains(player.getName())){
			File fileLocation = new File(plugin.getDataFolder() + File.separator + "TeamRed" + File.separator + "RedSpawns.yml");
			FileConfiguration redSpawns;
			redSpawns = YamlConfiguration.loadConfiguration(fileLocation);
			Random rand = new Random();
			int i1 = rand.nextInt(10) + 1;
			Double x = redSpawns.getDouble(i1 + ".x");
			Double y = redSpawns.getDouble(i1 + ".y");
			Double z = redSpawns.getDouble(i1 + ".z");
			float p = (float) redSpawns.getLong(i1 + ".pitch");
			float f = (float) redSpawns.getLong(i1 + ".yaw");
			Location spawn = event.getRespawnLocation();
			spawn.setX(x);
			spawn.setY(y);
			spawn.setZ(z);
			spawn.setPitch(p);
			spawn.setYaw(f);
			event.setRespawnLocation(spawn);
			player.getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
			player.getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
			player.getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
			player.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));
			player.getInventory().setItem(0, new ItemStack(Material.WOOD_SWORD));
		}
	}

    @EventHandler
    public void shopInteract(PlayerInteractEntityEvent event){
        Player player = event.getPlayer();
        if(event.getRightClicked().getType() == EntityType.VILLAGER){
            player.openInventory(plugin.categoryShopInventory);
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        if(event.getItem().getItemMeta().getDisplayName().equals())
    }

}
