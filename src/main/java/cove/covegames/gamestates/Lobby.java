package cove.covegames.gamestates;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import cove.covegames.Main;

public class Lobby extends BukkitRunnable{
	
	Main plugin;
	
	
	
	public static boolean lobbyMode = true;

	public Lobby(Main main) {
		plugin = main;
	}

	@Override
	public void run() {
		if(plugin.lobbyTime != 0){
			plugin.lobbyTime--;
			
			if(plugin.lobbyTime == 120){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "2" + ChatColor.GRAY + " minutes!");
				
			}
			if(plugin.lobbyTime == 90){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "1.5" + ChatColor.GRAY + " minutes!");
			}
			if(plugin.lobbyTime == 60){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "1" + ChatColor.GRAY + " minute!");
			}
			if(plugin.lobbyTime == 30){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "30" + ChatColor.GRAY + " seconds!");
			}
			if(plugin.lobbyTime == 15){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "15" + ChatColor.GRAY + " seconds!");
			}
			if(plugin.lobbyTime == 10){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "10" + ChatColor.GRAY + " seconds!");
			}
			if(plugin.lobbyTime == 5){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "5" + ChatColor.GRAY + " seconds!");
			}
			if(plugin.lobbyTime == 4){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "4" + ChatColor.GRAY + " seconds!");
			}
			if(plugin.lobbyTime == 3){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "3" + ChatColor.GRAY + " seconds!");
			}
			if(plugin.lobbyTime == 2){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "2" + ChatColor.GRAY + " seconds!");
			}
			if(plugin.lobbyTime == 1){
				plugin.getServer().broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game starting in " + ChatColor.GREEN + ChatColor.BOLD + "1" + ChatColor.GRAY + " seconds!");
			}
			if(plugin.lobbyTime == 0){
				if(Bukkit.getOnlinePlayers().length >= 16){
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "start");
				} else{
					plugin.lobbyTime = 120;
					Bukkit.broadcastMessage(plugin.prefix + ChatColor.RED + "There were not enough players to start the game! 16 is needed, restarting timer.");
				}
				
			}
		
		
	}

}
}
