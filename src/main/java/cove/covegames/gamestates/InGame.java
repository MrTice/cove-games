package cove.covegames.gamestates;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import cove.covegames.Main;

public class InGame extends BukkitRunnable{
	
	Main plugin;

	public InGame(Main main) {
		plugin = main;
	}

	@Override
	public void run() {
		plugin.timer--;
		if(plugin.timer == 1500){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + plugin.timer/60 + ChatColor.GRAY + " minutes!");
		}
		if(plugin.timer == 1200){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + plugin.timer/60 + ChatColor.GRAY + " minutes!");
		}
		if(plugin.timer == 900){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + plugin.timer/60 + ChatColor.GRAY + " minutes!");
		}
		if(plugin.timer == 600){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + plugin.timer/60 + ChatColor.GRAY + " minutes!");
		}
		if(plugin.timer == 300){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + plugin.timer/60 + ChatColor.GRAY + " minutes!");
		}
		if(plugin.timer == 60){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + 1 + ChatColor.GRAY + " seconds!");
		}
		if(plugin.timer == 30){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + 30 + ChatColor.GRAY + " seconds!");
		}
		if(plugin.timer == 15){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + 15 + ChatColor.GRAY + " seconds!");
		}
		if(plugin.timer == 10){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + 10 + ChatColor.GRAY + " seconds!");
		}
		if(plugin.timer == 5){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + 5 + ChatColor.GRAY + " seconds!");
		}
		if(plugin.timer == 4){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + 4 + ChatColor.GRAY + " seconds!");
		}
		if(plugin.timer == 3){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + 3 + ChatColor.GRAY + " seconds!");
		}
		if(plugin.timer == 2){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + 2 + ChatColor.GRAY + " seconds!");
		}
		if(plugin.timer == 1){
			Bukkit.broadcastMessage(plugin.prefix + ChatColor.GRAY + "Game ending in " + ChatColor.GREEN + ChatColor.BOLD + 1 + ChatColor.GRAY + " seconds!");
		}
		if(plugin.timer == 0){
			
		}
		
		
	}

}
