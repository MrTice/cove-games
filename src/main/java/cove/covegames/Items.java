package cove.covegames;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import cove.covegames.gamestates.listeners.LobbyListener;

public class Items {
	LobbyListener lobbyListener;
	
	
	
	public ItemStack switchTeam(){
		List<String> lores = new ArrayList<String>();

		lores.clear();
		lores.add("" + ChatColor.WHITE  + "" + ChatColor.YELLOW + "You may only change cove.covegames.teams once.");
		lores.add("" + ChatColor.WHITE  + "" + ChatColor.YELLOW + "Premiums can change cove.covegames.teams whenever.");
		lores.add("" + ChatColor.WHITE  + "" + ChatColor.YELLOW + "For premium, visit: " + ChatColor.GREEN + "http://shop.covenetwork.net/");

		ItemStack item = new ItemStack(Material.NETHER_STAR, 1);
		ItemMeta hm = item.getItemMeta();

		hm.setDisplayName("" + ChatColor.GRAY + ChatColor.BOLD + "Switch Team");
		hm.setLore(lores);

		item.setItemMeta(hm);

		return item;
		
	}
	public ItemStack createParty(){
		List<String> lores = new ArrayList<String>();

		lores.clear();
		lores.add("" + ChatColor.WHITE + ChatColor.ITALIC + "Create a party for your friends to join!");

		ItemStack item = new ItemStack(Material.CAULDRON_ITEM, 1);
		ItemMeta hm = item.getItemMeta();

		hm.setDisplayName("" + ChatColor.GOLD + ChatColor.BOLD + "Create Party");
		hm.setLore(lores);

		item.setItemMeta(hm);

		return item;
	}
	public ItemStack manageParty(){
		List<String> lores = new ArrayList<String>();

		lores.clear();
		lores.add("" + ChatColor.WHITE + ChatColor.ITALIC + "Kick, close or invite players to your party");

		ItemStack item = new ItemStack(Material.CAULDRON_ITEM, 1);
		ItemMeta hm = item.getItemMeta();

		hm.setDisplayName("" + ChatColor.GOLD + ChatColor.BOLD + "Manage Party");
		hm.setLore(lores);

		item.setItemMeta(hm);

		return item;
	}
	public ItemStack invitePartyItem(){
		List<String> lores = new ArrayList<String>();

		lores.clear();
		lores.add("" + ChatColor.WHITE + ChatColor.ITALIC + "Invite friends or others to join your party!");

		ItemStack item = new ItemStack(Material.EMERALD, 1);
		ItemMeta hm = item.getItemMeta();

		hm.setDisplayName("" + ChatColor.GREEN + ChatColor.BOLD + "Invite");
		hm.setLore(lores);

		item.setItemMeta(hm);

		return item;
	}
	public ItemStack kickPartyPlayer(){
		List<String> lores = new ArrayList<String>();

		lores.clear();
		lores.add("" + ChatColor.WHITE + ChatColor.ITALIC + "Kick out a player in your party");

		ItemStack item = new ItemStack(Material.SKULL, 1);
		ItemMeta hm = item.getItemMeta();

		hm.setDisplayName("" + ChatColor.RED + ChatColor.BOLD + "Kick Players");
		hm.setLore(lores);

		item.setItemMeta(hm);

		return item;
	}
	public ItemStack closePartyItem(){
		List<String> lores = new ArrayList<String>();

		lores.clear();
		lores.add("" + ChatColor.WHITE + ChatColor.ITALIC + "Close your party");
		lores.add("" + ChatColor.YELLOW + ChatColor.ITALIC + "Note that unless having premium, you can only create a party once.");

		ItemStack item = new ItemStack(Material.BED, 1);
		ItemMeta hm = item.getItemMeta();

		hm.setDisplayName("" + ChatColor.RED + ChatColor.BOLD + "Close Party");
		hm.setLore(lores);

		item.setItemMeta(hm);

		return item;
	}
	public ItemStack closeItem(){
		List<String> lores = new ArrayList<String>();

		lores.clear();

		ItemStack item = new ItemStack(Material.BOOK, 1);
		ItemMeta hm = item.getItemMeta();

		hm.setDisplayName("" + ChatColor.RED + ChatColor.BOLD + "Close");
		hm.setLore(lores);

		item.setItemMeta(hm);

		return item;
	}

}
