package cove.covegames.teams;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class Team {
	
	public static List<String> teamBlue = new ArrayList<String>();
	public static List<String> teamRed = new ArrayList<String>();
	
	
	public static void addToTeam(TeamType type, Player player){
		switch (type){
		case BLUE:
			teamBlue.add(player.getName());
			break;
		case RED:
			teamRed.add(player.getName());
			break;
		}
	}
	
	public static boolean isInTeam(Player player){
		return teamBlue.contains(player.getName()) || teamRed.contains(player.getName());
	}
	
	public static void clearTeams(){
		teamBlue.clear();
		teamRed.clear();
	}
    
	public List<String> getBlueTeam(){
		return teamBlue;
	}
	public List<String> getRedTeam(){
		return teamRed;
	}
	public static TeamType getTeamType(Player player){
		if(!isInTeam(player)) return null;
		return (teamBlue.contains(player.getName()) ? TeamType.BLUE : TeamType.RED);
	}

}
