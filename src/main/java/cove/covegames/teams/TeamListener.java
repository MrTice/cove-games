package cove.covegames.teams;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import cove.covegames.Main;

public class TeamListener implements Listener{
	
	Main plugin;
	
	
	public TeamListener(Main main) {
		main = plugin;
	}

    @EventHandler
	public void teamDivider(PlayerLoginEvent event){
    	System.out.println("It identifies the PlayerLoginEvent from TeamListener.");
		Player player = event.getPlayer();
		if(Team.teamBlue.size() == 0 && Team.teamRed.size() == 0){
			Random rand = new Random();
			int team = rand.nextInt(2) + 1;
			if(team == 1){
				Team.addToTeam(TeamType.BLUE, player);
				System.out.println("BLUE");
			} else if(team == 2){
				Team.addToTeam(TeamType.RED, player);
				System.out.println("RED");
				
			}
			return;
			
		}
		
		else if(Team.teamBlue.size() >= Team.teamRed.size()){
			Team.teamBlue.add(player.getName());
			System.out.println(Team.teamBlue.size());
			System.out.println("It kept going disregarding the return! :o");
		} else if(Team.teamRed.size() >= Team.teamBlue.size()){
			Team.teamRed.add(player.getName());
			
			
			System.out.println(Team.teamRed.size());
		}
	}
    @EventHandler
	public void teamDismantler(PlayerQuitEvent event){
		Player player = event.getPlayer();
		if(Team.teamBlue.contains(player.getName())){
			Team.teamBlue.remove(player.getName());
			System.out.println(Team.teamBlue.size());
		} else if(Team.teamRed.contains(player.getName())){
			Team.teamRed.remove(player.getName());
			System.out.println(Team.teamRed.size());
		}
	}


}
