package cove.covegames;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import cove.covegames.commands.AdminModeCMD;
import cove.covegames.commands.Commands;
import cove.covegames.commands.CreateDirectoriesCMD;
import cove.covegames.commands.SetShopCMD;
import cove.covegames.commands.StartCMD;
import cove.covegames.commands.TeamBlueCMD;
import cove.covegames.commands.TeamRedCMD;
import cove.covegames.gamestates.End;
import cove.covegames.gamestates.InGame;
import cove.covegames.gamestates.Lobby;
import cove.covegames.gamestates.listeners.InGameListener;
import cove.covegames.gamestates.listeners.LobbyListener;
import cove.covegames.listeners.DonorJoinListener;
import cove.covegames.listeners.GeneralListener;
import cove.covegames.listeners.JoinListener;
import cove.covegames.teams.Team;
import cove.covegames.teams.TeamListener;
import cove.covegames.parties.PartyHandler;

public class Main extends JavaPlugin{
	
	public String prefix = ChatColor.DARK_GRAY +"[" + ChatColor.BLUE + ChatColor.BOLD + "CV" + ChatColor.GOLD + ChatColor.BOLD + "N" + ChatColor.DARK_GRAY + "]: " + ChatColor.GOLD;
	public String noperm = prefix + ChatColor.RED + "This command is only available for Administrators.";
	
	
	//LISTENERS
	private JoinListener joinListener = new JoinListener(this);
    private DonorJoinListener donorListener = new DonorJoinListener(this);
    private InGameListener inGameListener = new InGameListener(this);
    private TeamListener teamListener = new TeamListener(this);
    private LobbyListener lobbyListener = new LobbyListener(this);
    private GeneralListener generalListener = new GeneralListener(this);

    //COMMANDS
    private Commands commands = new Commands(this);
    private TeamBlueCMD teamBlueCMD = new TeamBlueCMD(this);
    private TeamRedCMD teamRedCMD = new TeamRedCMD(this);
    private CreateDirectoriesCMD createdirsCMD = new CreateDirectoriesCMD(this);
    private AdminModeCMD adminModeCMD = new AdminModeCMD(this);
    private SetShopCMD setshopCMD = new SetShopCMD(this);
    private StartCMD startCMD = new StartCMD(this);
	
	public int timer = 1800;
	public int lobbyTime = 120;
	
	//ITEM CLASSES
	private Items items = new Items();
	
	//GAMESTATES
	public Lobby lobbyState = new Lobby(this);
	public InGame ingameState = new InGame(this);
	public End endGameState = new End(this);
	
	public DeBuggingRunnable deBuggingRunnable = new DeBuggingRunnable(this);
	
	//INVENTORIES
	public Inventory partyInventory;
	public Inventory managePartyInventory;
	public Inventory legendaryShopInventory;
    public Inventory categoryShopInventory;
	
	public void onEnable(){
		System.out.println("Cove games enabled!");
	    lobbyState.runTaskTimer(this, 0, 20);
	    deBuggingRunnable.runTaskTimer(this, 1, 40);
	    getServer().getPluginManager().registerEvents(joinListener, this);
	    getServer().getPluginManager().registerEvents(donorListener, this);
	    getServer().getPluginManager().registerEvents(teamListener, this);
	    getServer().getPluginManager().registerEvents(inGameListener, this);
	    getServer().getPluginManager().registerEvents(lobbyListener, this);
	    getServer().getPluginManager().registerEvents(generalListener, this);
	    
	    getCommand("setlobby").setExecutor(commands);
	    getCommand("setcenter").setExecutor(commands);
	    getCommand("start").setExecutor(startCMD);
	    getCommand("addbluespawn").setExecutor(teamBlueCMD);
	    getCommand("firstbluespawn").setExecutor(teamBlueCMD);
	    getCommand("addredspawn").setExecutor(teamRedCMD);
	    getCommand("createdirs").setExecutor(createdirsCMD);
	    getCommand("adminmode").setExecutor(adminModeCMD);
	    getCommand("setshop").setExecutor(setshopCMD);
	    saveConfig();
	    generateInventory();
	}
	
	public void onDisable(){
		System.out.println("Cove games disabled!");
		for(Entity e : Bukkit.getWorld("world").getEntities()){
			e.remove();
		}
		Team.clearTeams();
		lobbyListener.hasCreatedParty.clear();
		PartyHandler.parties.clear();
	}
	public Player getPlayer(String s){
		for(Player p : Bukkit.getOnlinePlayers()){
			if(p.getName().equalsIgnoreCase(s)) return p;
		}
		return null;
	}
	
	public String getRankColor(Player p) {
		if (p.hasPermission("covechat.adminchat")) {
			return "�4�l";
		}
		if (p.hasPermission("covechat.srmod")) {
			return "�4";
		}
		if (p.hasPermission("covechat.modchat")) {
			return "�c";
		}
		if (p.hasPermission("covechat.emeraldchat")) {
			return "�a";
		}
		if (p.hasPermission("covechat.diamondchat")) {
			return "�b";
		}
		if (p.hasPermission("covechat.goldchat")) {
			return "�6";
		}
		if (p.hasPermission("covechat.ironchat")) {
			return "�7";
		}
		if (p.hasPermission("covechat.vipchat")) {
			return "�5";
		}
		if (p.hasPermission("covechat.friendchat")) {
			return "�d";
		}
		if (p.hasPermission("covechat.developerchat")) {
			List<ChatColor> colors = new ArrayList<ChatColor>();
			colors.add(ChatColor.GOLD);
			colors.add(ChatColor.RED);
			colors.add(ChatColor.GREEN);
			colors.add(ChatColor.YELLOW);
			colors.add(ChatColor.DARK_GREEN);
			String n = "";
			String s = "";
			for (char c : s.toCharArray()) {
				Random ran = new Random();
				int ci = ran.nextInt(colors.size());
				ChatColor color = (ChatColor) colors.toArray()[ci];
				n = n + color + ChatColor.BOLD + c;
			}
			return n;
		}
		if (p.hasPermission("covechat.ownerchat")) {
			return "�4�l";
		}
		return "�9";

	}
	public void generateInventory(){
		managePartyInventory = Bukkit.getServer().createInventory(null,  9, "Manage party");
		managePartyInventory.setItem(0, items.invitePartyItem());
		managePartyInventory.setItem(1, items.kickPartyPlayer());
		managePartyInventory.setItem(2, items.closePartyItem());
		managePartyInventory.setItem(8, items.closeItem());
	}
	
	
	
	
	
	
	
	

}
