package cove.covegames.parties;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Party {
	
	private int maximumPlayers;
	private boolean allowsChallenging = true;
	private String name;
	private String teamPrefix;
	private ArrayList<String> players;
	private int roundsWon;
	
	public Party(String name, int max){
		this.name = name;
		this.players = new ArrayList<String>();
		this.teamPrefix = "TestTeam";
		this.maximumPlayers = max;
		this.roundsWon = 0;
	}
	
	public Party(String name, ArrayList<Player> players, int max){
		this.name = name;
		for(Player p : players){
			this.players.add(p.getName());
		}
		this.teamPrefix = "TestTeam";
		this.maximumPlayers = max;
	}
	
	public int getSize(){
		return players.size();
	}
	
	
	public String getName(){
		return name;
	}
	
	public String getTeamPrefix(){
		return teamPrefix;
	}
	
	public void setTeamPrefix(String prefix){
		this.teamPrefix = prefix;
	}
	
	public void addPlayer(Player player){
		if(players.size()+1 <= maximumPlayers){
			players.add(player.getName());
		}
	}
	
	@Deprecated
	public void addPlayer(String playerName){
		if(players.size()+1 <= maximumPlayers){
			players.add(playerName);
		}
	}
	
	public void removePlayer(Player p){
		players.remove(p);
	}
	
	public ArrayList<String> getPlayerNames(){
		return players;
	}
	
	public Set<Player> getPlayers(){
		Set<Player> playerSet = new HashSet<Player>();
		for(Player p : Bukkit.getOnlinePlayers()){
			if(players.contains(p.getName())){
				playerSet.add(p);
			}
		}
		return playerSet;
	}
	
	public int getMaximumPlayers(){
		return maximumPlayers;
	}
	
	public boolean allowsChallenging(){
		return allowsChallenging;
	}
	
	public void allowChallenging(boolean challenging){
		this.allowsChallenging = challenging;
	}
	
	public void sendMessage(String message){
		for(Player p : this.getPlayers()){
			p.sendMessage(message);
		}
	}
	
	public boolean isFull(){
		if(players.size() == maximumPlayers){
			return true;
		}else{
			return false;
		}
	}
	
	public boolean containsPlayer(Player p){
		if(players.contains(p.getName())){
			return true;
		}else{
			return false;
		}
	}
	
	public int getRoundsWon(){
		return roundsWon;
	}
	
	public void setRoundsWon(int rounds){
		this.roundsWon = rounds;
	}

}
