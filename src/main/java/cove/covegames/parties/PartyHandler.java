package cove.covegames.parties;

import java.util.HashMap;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import cove.covegames.Main;

public class PartyHandler {

	private Main pl;
	public static HashMap<String, Party> parties = new HashMap<String, Party>();

	
	public PartyHandler(Main plugin){
		pl = plugin;
	}
	
	public void addParty(String name, int maximumPlayers){
		
			Party party = new Party(name, maximumPlayers);
			parties.put(name, party);
		
		
	}
	
	public Party getParty(String name){
		return parties.get(name);
	}
	
	public void addPlayerToParty(String teamName, Player player){
		if(!getParty(teamName).isFull()){
			getParty(teamName).addPlayer(player);
		}else{
			player.sendMessage(pl.prefix + ChatColor.RED + "You can't join the team because it's full!");

		}
	}
	
	public void addPlayerToTeam(String partyName, String playerName){
		if(!getParty(partyName).isFull()){
			getParty(partyName).addPlayer(playerName);
		}else{
			pl.getPlayer(playerName).sendMessage(pl.prefix + ChatColor.RED + "You can't join the team because it's full!");
		}
	}
	
	public void removeParty(String name){
		parties.remove(name);
	}
	
	public boolean isInParty(Player p){
		boolean r = false;
		for(Party party : parties.values()){
			if(party.containsPlayer(p)){
				r = true;
			}
		}
		return r;
	}
	
	public boolean isInSameParty(Player p, Player d){
		boolean b = false;
		for(Party party : parties.values()){
			if(party.containsPlayer(p) && party.containsPlayer(d)){
				b = true;
			}
		}
		return b;
	}
	
	public Party getPlayersParty(Player p){
		Party pa = null;
		for(Party party : parties.values()){
			if(party.containsPlayer(p)){
				pa = party;
			}
		}
		return pa;
	}
}