package cove.covegames.listeners;


import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import cove.covegames.Main;

public class DonorJoinListener implements Listener{
	
	public static HashSet<String> kickTag = new HashSet<String>();
	Main plugin;
	
	public DonorJoinListener(Main main) {
		plugin = main;
	}
	@EventHandler
	public void donorJoin(PlayerLoginEvent event){
		if(plugin.getServer().getOnlinePlayers().length == 30){
			if(event.getPlayer().hasPermission("cove.donor")){
				for(Player onlinePlayers : Bukkit.getOnlinePlayers()){
					if(kickTag.contains(onlinePlayers)){
						onlinePlayers.kickPlayer(ChatColor.RED + "You have been kicked in order to make room for a donor, sorry!");
					}
				}
			} else{
				
			}
		}
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		if(plugin.getServer().getOnlinePlayers().length == 29){
			if(!event.getPlayer().hasPermission("cove.donor")){
				kickTag.clear();
			    kickTag.add(event.getPlayer().getUniqueId().toString().replace("-", ""));
			} else{
				
			}
			
			
		
		}
		
		
	}
	
	
	

}
