package cove.covegames.listeners;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

import cove.covegames.Items;
import cove.covegames.Main;

public class JoinListener implements Listener{
	
	Main plugin;
	Items items = new Items();
	
	public JoinListener(Main main) {
		plugin = main;
	}
	
	//@EventHandler
	//public void onJoin(PlayerJoinEvent event){
		//Player player = event.getPlayer();
		//event.setJoinMessage(plugin.prefix + player.getName() + ChatColor.WHITE + " has joined.");
	//}
	@EventHandler
	public void onLogin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		World world = Bukkit.getWorld("world");
		
		//if(Bukkit.getServer().getOnlinePlayers().length >= 30 && !player.hasPermission("cove.donor")){
			//event.disallow(Result.KICK_FULL, ChatColor.RED + "The server is full!" + "\n\n" + ChatColor.GREEN + "Want to join full servers?" + "\n\n" + ChatColor.YELLOW + "Buy premium at BAJS");
		//}
		if(plugin.timer >= 1800){
			File locationFile = new File(plugin.getDataFolder() + "lobbySpawn.yml");
			YamlConfiguration lobbySpawn = YamlConfiguration.loadConfiguration(locationFile);
			String lobby = "lobby";
			Double x = lobbySpawn.getDouble(lobby + ".x");
			Double y = lobbySpawn.getDouble(lobby + ".y");
			Double z = lobbySpawn.getDouble(lobby + ".z");
			float p = (float) lobbySpawn.getLong(lobby + ".pitch");
			float f = (float) lobbySpawn.getLong(lobby + ".yaw");
			player.teleport(new Location(world, x, y, z, f, p));
			player.getInventory().clear();
			player.getInventory().setArmorContents(null);
			player.setGameMode(GameMode.ADVENTURE);
			player.setFoodLevel(20);
			player.setHealth(20);
			player.getInventory().setItem(0, items.switchTeam());
			player.sendMessage(plugin.prefix + ChatColor.YELLOW + "Welcome to Cove Games! Need help regarding how to play? Visit the information board!");
			event.setJoinMessage(plugin.prefix + plugin.getRankColor(player) + player.getName() + ChatColor.YELLOW +  " has joined.");
			System.out.println("END OF THE LOBBYHANDLER LOGIN STAGE");
			
			for (PotionEffect pe : player.getActivePotionEffects()) {
				player.removePotionEffect(pe.getType());
			}
			
		} else{
			if(player.hasPermission("cove.staff")){
				File locationFile = new File(plugin.getDataFolder() + "mapCenter.yml");
				YamlConfiguration mapCenter = YamlConfiguration.loadConfiguration(locationFile);
				String center = "center";
				Double x = mapCenter.getDouble(center + ".x");
				Double y = mapCenter.getDouble(center + ".y");
				Double z = mapCenter.getDouble(center + ".z");
				float p = (float) mapCenter.getLong(center + ".pitch");
				float f = (float) mapCenter.getLong(center + ".yaw");
				player.teleport(new Location(world, x, y, z, f, p));
				player.getInventory().clear();
				player.setGameMode(GameMode.SURVIVAL);
				player.setFlying(true);
				player.setFoodLevel(20);
				player.setHealth(20);
				for(PotionEffect pe : player.getActivePotionEffects()){
					player.removePotionEffect(pe.getType());
				}
				for(Player players : Bukkit.getOnlinePlayers()){
					players.hidePlayer(player);
				}
				player.sendMessage(plugin.prefix + ChatColor.YELLOW + "Welcome, dear staff member. You have been logged in secretly.");
				
				
				
				
			} else if(!player.hasPermission("cove.staff")){
				player.kickPlayer(ChatColor.RED + "You cannot join in-game!");
			}
		}
		
		
		
		
	}
	@EventHandler
	public void onLeave(PlayerQuitEvent event){
		Player player = event.getPlayer();
		event.setQuitMessage(plugin.prefix + player.getName() + ChatColor.WHITE + " has left.");
	}
	
//	public ItemStack switchTeam(){
//		List<String> lores = new ArrayList<String>();
//
//		lores.clear();
//		lores.add("" + ChatColor.WHITE  + "" + ChatColor.YELLOW + "You may only change cove.covegames.teams once.");
//		lores.add("" + ChatColor.WHITE  + "" + ChatColor.YELLOW + "Premiums can change cove.covegames.teams whenever.");
//		lores.add("" + ChatColor.WHITE  + "" + ChatColor.YELLOW + "For premium, visit: " + ChatColor.GREEN + "http://shop.covenetwork.net/");
//
//		ItemStack item = new ItemStack(Material.NETHER_STAR, 1);
//		ItemMeta hm = item.getItemMeta();
//
//		hm.setDisplayName("" + ChatColor.GRAY + ChatColor.BOLD + "Switch Team");
//		hm.setLore(lores);
//
//		item.setItemMeta(hm);
//
//		return item;
//
//	}

}
