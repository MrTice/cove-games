package cove.covegames.listeners;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import cove.covegames.Main;
import cove.covegames.commands.AdminModeCMD;
import cove.covegames.teams.Team;

public class GeneralListener implements Listener{
	Main plugin;
	AdminModeCMD adminModeCMD;
	
	public GeneralListener(Main main) {
		plugin = main;
	}
	//@EventHandler
//	public void onBlockBreak(BlockBreakEvent event){
		//if(adminModeCMD.adminMode.contains(event.getPlayer().getUniqueId().toString().replace("-", ""))){
		//	event.setCancelled(false);
		//} else{
		//	event.setCancelled(true);
		//}
	//}
	//@EventHandler
	//public void onBlockPlace(BlockPlaceEvent event){
		//if(adminModeCMD.adminMode.contains(event.getPlayer().getUniqueId().toString().replace("-", ""))){
		//	event.setCancelled(false);
		//} else{
		//	event.setCancelled(true);
		//}
	//}
	@EventHandler
	public void foodChange(FoodLevelChangeEvent event){
		event.setCancelled(true);
	}
	@EventHandler
	public void leafDecay(LeavesDecayEvent event){
		event.setCancelled(true);
	}
	@EventHandler
	public void playerChatFormat(AsyncPlayerChatEvent e) {

		Player p = e.getPlayer();
		String msg = e.getMessage();

		if (p.hasPermission("covechat.adminchat")) {
			e.setFormat("" + ChatColor.DARK_RED + ChatColor.BOLD + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.AQUA + msg);
		} else if (p.hasPermission("covechat.developerchat")) {
			e.setFormat("" + ChatColor.DARK_AQUA + ChatColor.BOLD + colorTextDev(p.getName()) + ChatColor.GREEN + ": " + ChatColor.AQUA
					+ ChatColor.BOLD + msg);
		} else if (p.hasPermission("covechat.srmod")) {
			e.setFormat("" + ChatColor.DARK_RED + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.AQUA + msg);
		} else if (p.hasPermission("covechat.modchat")) {
			e.setFormat("" + ChatColor.RED + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.AQUA + msg);
		} else if (p.hasPermission("covechat.emeraldchat")) {
			e.setFormat("" + ChatColor.GREEN + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);
		} else if (p.hasPermission("covechat.diamondchat")) {
			e.setFormat("" + ChatColor.AQUA + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);
		} else if (p.hasPermission("covechat.goldchat")) {
			e.setFormat("" + ChatColor.GOLD + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);
		} else if (p.hasPermission("covechat.ironchat")) {
			e.setFormat("" + ChatColor.GRAY + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);
		} else if (p.hasPermission("covechat.vipchat")) {
			e.setFormat("" + ChatColor.DARK_PURPLE + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);
		} else if (p.hasPermission("covechat.friendchat")) {
			e.setFormat("" + ChatColor.LIGHT_PURPLE + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);
		} else if (p.hasPermission("covechat.ownerchat")) {
			e.setFormat("" + ChatColor.DARK_RED + ChatColor.BOLD + p.getName() + ChatColor.GOLD + ": " + ChatColor.AQUA + ChatColor.BOLD + msg);
		} else if (p.isOp() || p.hasPermission("*")) {
			e.setFormat("" + ChatColor.DARK_RED + ChatColor.BOLD + p.getName() + ChatColor.GOLD + ": " + ChatColor.AQUA + ChatColor.BOLD + msg);
		} else if (p.hasPermission("covechat.defaultchat")) {
			e.setFormat("" + ChatColor.BLUE + p.getName() + ChatColor.DARK_GRAY + ": " + ChatColor.WHITE + msg);
		}

	}
	public String colorTextDev(String s) {
		List<ChatColor> colors = new ArrayList<ChatColor>();
		colors.add(ChatColor.GOLD);
		colors.add(ChatColor.RED);
		colors.add(ChatColor.GREEN);
		colors.add(ChatColor.YELLOW);
		colors.add(ChatColor.DARK_GREEN);
		String n = "";
		for (char c : s.toCharArray()) {
			Random ran = new Random();
			int ci = ran.nextInt(colors.size());
			ChatColor color = (ChatColor) colors.toArray()[ci];
			n = n + color + ChatColor.BOLD + c;
		}
		return n;
	}

}
